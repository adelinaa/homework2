public class Main {
    public static void main(String[] args) {
        Dog dog1 = new Dog("Rex", 4, "Pug");
        Cat cat1 = new Cat("Pheobe", 4, true);
        Animal[] animals = {dog1, cat1};
        for(Animal animal : animals){
            animal.yieldVoice();
        }
    }
}
