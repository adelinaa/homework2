public class Dog extends Animal {
    private String race;

    public Dog(String name, int numberOfLegs, String race) {
        super(name, numberOfLegs);
        this.race = race;
    }

    public String getRace(){
        return this.race;
    }

    public void setRace(String race){
        this.race = race;
    }

    @Override
    public void yieldVoice() {
        System.out.println("Ham Ham!");
    }
}
