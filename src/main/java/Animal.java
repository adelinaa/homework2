public class Animal {
    private String name;
    private int numberOfLegs;

    public Animal(String name, int numberOfLegs){
        this.name = name;
        this.numberOfLegs = numberOfLegs;
    }

    public String getName(){
        return this.name;
    }

    public void setName(String name){
        this.name = name;
    }

    public int getNumberOfLegs(){
        return this.numberOfLegs;
    }

    public void setNumberOfLegs(int numberOfLegs){
        this.numberOfLegs = numberOfLegs;
    }

    public void yieldVoice(){
        System.out.println("The animal talks.");
    }
}
