public class Cat extends Animal {
    private boolean happy;

    public Cat(String name, int numberOfLegs, boolean happy) {
        super(name, numberOfLegs);
        this.happy = happy;
    }

    public boolean ishappy(){
        return this.happy;
    }

    public void sethappy(boolean happy){
        this.happy = happy;
    }
    @Override
    public void yieldVoice(){
        System.out.println("Meow Meow!");
    }
}
